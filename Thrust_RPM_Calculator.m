clear
clc

%example file name
%P480_8x4_0.18A_1800_0.dat
%Motor, Prop, Amps, RPM, Labjack extension

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                          %%%
%%%           NOTE:          %%%
%%%                          %%%
%%% THIS CODE ONLY WORKS FOR %%%
%%% ONE MOTOR AT THE MOMENT. %%%
%%% DO NOT INPUT MORE THAN   %%%
%%% ONE MOTOR. IT WILL CRASH %%%
%%%                          %%%
%%% Last rev 11/29/2018      %%%
%%% 8:43 PM by Conor McC.    %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

STAND_CONST = 20.52386283; %lbf/V
DENS = 0.002377; %slug/ft^3
VOLT = 11.1; %3S

files = dir('*.dat');
names = [];
for i = 1:length(files)
    names{i} = files(i).name;
end

data = cell(length(names),5); %5 columns for 5 data points

for i = 1:length(names)
   
    a = importdata(names{i},'\t');
    a = a.data;
    avg = mean(a(:,2));
    
    arr = strsplit(names{i},'_');
    
    data{i,1} = arr{1};
    data{i,2} = arr{2};
    data{i,3} = str2double(erase(arr{3},'A'));
    data{i,4} = str2double(arr{4});
    data{i,5} = avg;  
    
end

motors = unique(data(:,1));
props = cell(1,length(motors));

for i = 1:length(motors)
   props(i) = cell(1); 
end

for i = 1:length(names)

    for j = 1:length(motors)
       
        if strcmp(motors{j},data{i,1})
            
            props{j}{length(props{j})+1} = data{i,2};
            props{j} = unique(props{j});
            
        end
    end
    
end

props = reshape(props{1},[length(props{1}),1]);
array = [];
for i = 1:length(motors)
   
    for j = 1:length(props(:,1))
        
        for k = 1:length(data(:,1))
        
            if strcmp(motors{i},data{k,1}) && strcmp(props{j,1},data{k,2})
                
                array = [array; data{k,3} data{k,4} data{k,5}];
                
            end
            
        end

        
        if ~isempty(array)
            
            zero = array(array(:,2) == 0,3);
            v0 = mean(zero);
                
            array(array(:,2) == 0,:) = [];
            array = [0,0,v0; array];
        
            for k = 2:length(array(:,1))
                array(k,3) = STAND_CONST*(array(1,3) - array(k,3));
                if array(k,3) < 0
                    array(k,3) = 0;
                end
            end
            array(1,3) = 0;
        
            props{j,2} = array;
            array = [];
        
        end
        
    end
    
end

for i = 1:length(props(:,1))
   
   D = strsplit(props{i,1},'x');
   D = (1/12)*str2double(D{1}); %prop diameter in ft
   
   CtCp = [];
   for j = 1:length(props{i,2}(:,1))
       
       n = props{i,2}(j,2)*2*pi/60; %RPM to rad/s
       Cp = (props{i,2}(j,1)*VOLT*0.73756215)/(DENS*D^5*n^3);
       Ct = props{i,2}(j,3)/(DENS*n^2*D^4);
    
       CtCp = [CtCp; Ct, Cp];
       
   end
   
   props{i,2} = [props{i,2} CtCp]; %combine arrays
   
   figure
   scatter(props{i,2}(:,2),props{i,2}(:,4)); %Ct
   hold on
   scatter(props{i,2}(:,2),props{i,2}(:,5)); %Cp
   title(['Ct and Cp vs Prop RPM for ' props{i,1}]);
   legend('Ct','Cp');
   xlabel('RPM')
   %savefig([props{i,1} '.fig']);
   saveas(gcf,[props{i,1} '.jpg']);
   
   xlswrite('processed.xlsx',props{i,2},props{i,1});
   xlswrite('processed.xlsx',["Current (A)","RPM","Thrust (lbf)","Ct","Cp"],props{i,1});
   
end



